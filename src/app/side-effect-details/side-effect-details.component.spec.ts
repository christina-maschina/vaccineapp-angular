import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SideEffectDetailsComponent } from './side-effect-details.component';

describe('SideEffectDetailsComponent', () => {
  let component: SideEffectDetailsComponent;
  let fixture: ComponentFixture<SideEffectDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SideEffectDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SideEffectDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
