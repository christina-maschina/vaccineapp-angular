import {Component, Input, OnInit} from '@angular/core';
import {SideEffectService} from '../services/side-effect.service';
import {ActivatedRoute} from '@angular/router';
import {SideEffect} from '../models/SideEffect';
import {Location} from '@angular/common';
import {VaccineService} from '../services/vaccine.service';
import {Vaccine} from '../models/vaccine';

@Component({
  selector: 'app-side-effect-details',
  templateUrl: './side-effect-details.component.html',
  styleUrls: ['./side-effect-details.component.css']
})
export class SideEffectDetailsComponent implements OnInit {
  sideEffect: SideEffect | undefined;
  @Input() vaccine: Vaccine | undefined;

  constructor(private sideEffectService: SideEffectService,
              private route: ActivatedRoute,
              private location: Location) {
  }

  ngOnInit(): void {
    this.getSideEffectByShortDescription();
  }

  private getSideEffectByShortDescription(): void {
    const routeParam = this.route.snapshot.paramMap.get('shortDescription');
    this.sideEffectService.getSideEffectByShortDescription(routeParam).subscribe(sideEffect => this.sideEffect = sideEffect);
  }

  goBack(): void {
    this.location.back();
  }

}
