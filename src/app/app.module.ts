import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule, routingComponents } from './app-routing.module';

import {AppComponent} from './app.component';
import { MessageComponent } from './message/message.component';
import {HttpClientModule} from '@angular/common/http';
import { FormComponent } from './form/form.component';
import {FormsModule} from '@angular/forms';
import { SideEffectResearchNameComponent } from './side-effect-research-name/side-effect-research-name.component';
import { SearchSideEffectComponent } from './search-side-effect/search-side-effect.component';
import { SearchDetailsComponent } from './search-details/search-details.component';

@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    MessageComponent,
    FormComponent,
    SideEffectResearchNameComponent,
    SearchSideEffectComponent,
    SearchDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
