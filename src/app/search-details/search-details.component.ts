import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-search-details',
  templateUrl: './search-details.component.html',
  styleUrls: ['./search-details.component.css']
})
export class SearchDetailsComponent implements OnInit {
  routeParam: string | null | undefined;
  constructor(private route: ActivatedRoute, private location: Location) {}

  ngOnInit(): void {
     this.routeParam = this.route.snapshot.paramMap.get('manufacturerName');
  }
  goBack(): void {
    this.location.back();
  }
}
