import {Component, OnInit} from '@angular/core';
import {Vaccine} from '../models/vaccine';
import {ActivatedRoute} from '@angular/router';
import {VaccineService} from '../services/vaccine.service';
import {Observable} from 'rxjs';
import {Location} from '@angular/common';

@Component({
  selector: 'app-vaccine-detail',
  templateUrl: './vaccine-detail.component.html',
  styleUrls: ['./vaccine-detail.component.css']
})
export class VaccineDetailComponent implements OnInit {

  vaccine: Vaccine | undefined;
  vaccines: Observable<Vaccine[]> | undefined;

  constructor(
    private vaccineService: VaccineService,
    private route: ActivatedRoute,
    private location: Location) {
  }

  ngOnInit(): void {
    this.getVaccineByResearchName();
  }

  getVaccineByResearchName(): void {
    const routeParam = this.route.snapshot.paramMap.get('researchName');
    this.vaccineService.getVaccineByResearchName(routeParam)
      .subscribe(vaccine => this.vaccine = vaccine);
  }

  goBack(): void {
    this.location.back();
  }
}
