export interface SideEffect {
   id: number;
   shortDescription: string;
   frequency: number;
   longDescription: string;
   vaccine_id: number;
}
