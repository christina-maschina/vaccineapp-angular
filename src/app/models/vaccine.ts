export interface Vaccine{
  id: number;
  researchName: string;
  manufacturerName: string;
  vaccineType: string;
  requiredDose: number;
  availableDose: number;
}

