import {Component, Input, OnInit} from '@angular/core';
import {SideEffectService} from '../services/side-effect.service';
import {SideEffect} from '../models/SideEffect';
import {Router} from '@angular/router';

@Component({
  selector: 'app-side-effect',
  templateUrl: './side-effect.component.html',
  styleUrls: ['./side-effect.component.css']
})
export class SideEffectComponent implements OnInit {
  sideEffects: SideEffect[] = [];
  selectedSideEffect: SideEffect | undefined;

  constructor(private sideEffectService: SideEffectService, private router: Router) {
  }

  ngOnInit(): void {
    this.getSideEffects();
  }

  private getSideEffects(): void {
    this.sideEffectService.getSideEffects().subscribe(sideEffects => this.sideEffects = sideEffects);
  }

  onSelect(sideEffect: SideEffect): void {
    this.selectedSideEffect = sideEffect;
    this.router.navigate(['sideEffects/', sideEffect.shortDescription]);
  }

}
