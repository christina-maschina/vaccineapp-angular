import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SideEffectResearchNameComponent } from './side-effect-research-name.component';

describe('SideEffectResearchNameComponent', () => {
  let component: SideEffectResearchNameComponent;
  let fixture: ComponentFixture<SideEffectResearchNameComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SideEffectResearchNameComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SideEffectResearchNameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
