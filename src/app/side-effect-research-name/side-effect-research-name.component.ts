import {Component, Input, OnInit} from '@angular/core';
import {SideEffect} from '../models/SideEffect';
import {SideEffectService} from '../services/side-effect.service';

@Component({
  selector: 'app-side-effect-research-name',
  templateUrl: './side-effect-research-name.component.html',
  styleUrls: ['./side-effect-research-name.component.css']
})
export class SideEffectResearchNameComponent implements OnInit {
  sideEffects: SideEffect[] = [];
  @Input() researchName: string | undefined;

  constructor(private sideEffectService: SideEffectService) { }

  ngOnInit(): void {
    this.getSideEffectsByResearchName();
  }

  public getSideEffectsByResearchName(): void {
    this.sideEffectService.getSideEffectsByResearchName(this.researchName).subscribe(sideEffect => this.sideEffects = sideEffect);
  }
}
