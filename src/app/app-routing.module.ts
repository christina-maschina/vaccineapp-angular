import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {VaccinesComponent} from './vaccines/vaccines.component';
import {VaccineDetailComponent} from './vaccine-detail/vaccine-detail.component';
import {SideEffectComponent} from './side-effect/side-effect.component';
import {SideEffectDetailsComponent} from './side-effect-details/side-effect-details.component';
import {SearchSideEffectComponent} from './search-side-effect/search-side-effect.component';
import {SearchDetailsComponent} from './search-details/search-details.component';

const routes: Routes = [
  {path: 'vaccines', component: VaccinesComponent},
  {path: 'details/:researchName', component: VaccineDetailComponent},
  {path: 'sideEffects', component: SideEffectComponent},
  {path: 'sideEffects/:shortDescription', component: SideEffectDetailsComponent},
  {path: 'search', component: SearchSideEffectComponent},
  {path: 'searchDetails/:manufacturerName', component: SearchDetailsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

export const routingComponents = [VaccinesComponent, VaccineDetailComponent, SideEffectComponent, SideEffectDetailsComponent];
