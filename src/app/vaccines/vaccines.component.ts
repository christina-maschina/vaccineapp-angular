import {Component, OnInit} from '@angular/core';
import {Vaccine} from '../models/vaccine';
import {VaccineService} from '../services/vaccine.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-vaccines',
  templateUrl: './vaccines.component.html',
  styleUrls: ['./vaccines.component.css']
})
export class VaccinesComponent implements OnInit {
  vaccines: Vaccine[] | undefined;
  selectedVaccine: Vaccine | undefined;

  constructor(private vaccineService: VaccineService, private router: Router) {
  }

  ngOnInit(): void {
    this.getVaccines();
  }

  getVaccines(): void {
    this.vaccineService.getVaccines()
      .subscribe(vaccines => this.vaccines = vaccines);
  }

  onSelect(vaccine: Vaccine): void {
    this.selectedVaccine = vaccine;
    this.router.navigate(['/details', vaccine.researchName]);
  }

  deleteVaccine(vaccine: Vaccine): void {
    this.vaccines = this.vaccines?.filter(v => v !== vaccine);
    this.vaccineService.deleteVaccine(vaccine).subscribe();
  }
}
