import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {Vaccine} from '../models/vaccine';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class VaccineService {

  private vaccinesUrl = 'http://localhost:8080/vaccine/';

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  constructor(private http: HttpClient) {
  }

  getVaccines(): Observable<Vaccine[]> {
    return this.http.get<Vaccine[]>(this.vaccinesUrl)
      .pipe(
        tap(_ => console.log('fetched vaccines')),
        catchError(this.handleError<Vaccine[]>('getVaccines', []))
      );
  }

  getVaccineByResearchName(param: string | null): Observable<Vaccine> {
    const url = `${this.vaccinesUrl}/${param}`;
    return this.http.get<Vaccine>(url)
      .pipe(
        tap(_ => console.log(`fetched vaccine researchName=${param}`)),
        catchError(this.handleError<Vaccine>('getVaccineByResearchName researchName=${id}'))
      );
  }

  deleteVaccine(param: Vaccine | string): Observable<Vaccine> {
    const researchName = typeof param === 'string' ? param : param.researchName;
    const url = `${this.vaccinesUrl}/${researchName}`;

    return this.http.delete<Vaccine>(url, this.httpOptions)
      .pipe(
        tap(_ => console.log(`deleted vaccine researchName=${researchName}`)),
        catchError(this.handleError<Vaccine>(`deleteVaccine researchName=${researchName}`))
      );
  }

  addVaccine(vaccine: Vaccine | undefined): Observable<Vaccine> {
    return this.http.post<Vaccine>(this.vaccinesUrl, vaccine, this.httpOptions)
      .pipe(
        tap((newVaccine: Vaccine) => console.log(`added vaccine researchName=${newVaccine.researchName}`)),
        catchError(this.handleError<Vaccine>(`addVaccine`)),
      );
  }

  getById(id: number | undefined): Observable<Vaccine> {
    const url = `${this.vaccinesUrl}id/${id}`;
    return this.http.get<Vaccine>(url)
      .pipe(
        tap(_ => console.log('fetched vaccine')),
        catchError(this.handleError<Vaccine>('getById'))
      );
  }

  // tslint:disable-next-line:typedef
  private handleError<T>(operation: string, result?: any[]) {
    return (error: any): Observable<T> => {
      console.error(operation);
      console.error(error);
      return of(result as unknown as T);
    };
  }
}
