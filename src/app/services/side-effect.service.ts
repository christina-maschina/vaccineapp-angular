import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {SideEffect} from '../models/SideEffect';

@Injectable({
  providedIn: 'root'
})
export class SideEffectService {
  private sideEffectUrl = 'http://localhost:8080/sideEffect';

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  constructor(private http: HttpClient) {
  }

  getSideEffects(): Observable<SideEffect[]> {
    return this.http.get<SideEffect[]>(this.sideEffectUrl, this.httpOptions)
      .pipe(
        tap(_ => console.log('fetched sideEffects')),
        catchError(this.handleError<SideEffect[]>('getSideEffects', []))
      );
  }

  getSideEffectsByResearchName(researchName: string | undefined): Observable<SideEffect[]> {
    const url = `${this.sideEffectUrl}/researchName/${researchName}`;
    return this.http.get<SideEffect[]>(url, this.httpOptions)
      .pipe(
        tap(_ => console.log(`fetched sideEffect with research name = ${researchName}`)),
        catchError(this.handleError<SideEffect[]>('getSideEffectByResearchName'))
      );
  }

  getSideEffectByShortDescription(shortDescr: string | null): Observable<SideEffect> {
    const url = `${this.sideEffectUrl}/${shortDescr}`;
    return this.http.get<SideEffect>(url, this.httpOptions)
      .pipe(
        tap(_ => console.log(`fetched sideEffect with short description = ${shortDescr}`)),
        catchError(this.handleError<SideEffect>('getSideEffectByShortDescription'))
      );
  }

  search(longDescription: string): Observable<SideEffect[]> {
    const url = `${this.sideEffectUrl}/search/${longDescription}`;
    return this.http.get<SideEffect[]>(url, this.httpOptions)
      .pipe(
        tap(_ => console.log(`fetched sideEffect with long description = ${longDescription}`)),
        catchError(this.handleError<SideEffect[]>('search', []))
      );
  }

  // tslint:disable-next-line:typedef
  private handleError<T>(operation: string, result?: any[]) {
    return (error: any): Observable<T> => {
      console.error(operation);
      console.error(error);
      return of(result as unknown as T);
    };
  }
}
