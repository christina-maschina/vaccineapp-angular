import {Component, OnInit} from '@angular/core';
import {VaccineService} from '../services/vaccine.service';
import {Vaccine} from '../models/vaccine';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css'],
})
export class FormComponent implements OnInit {
  types = ['Viral vector', 'mRNA'];
  vaccines: Vaccine[] | undefined;

  constructor(private vaccineService: VaccineService) {
  }

  ngOnInit(): void {
    this.getVaccines();
  }

  getVaccines(): void {
    this.vaccineService.getVaccines().subscribe(vaccines => this.vaccines = vaccines);
  }

  add(researchName: string, manufacturerName: string, vaccineType: string,
      requiredDose: number, availableDose: number): void {

    researchName = researchName.trim();
    manufacturerName = manufacturerName.trim();
    vaccineType = vaccineType.trim();

    if (!researchName || !manufacturerName || !vaccineType || !requiredDose || !availableDose) {
      return;
    }

    this.vaccineService.addVaccine({researchName, manufacturerName, vaccineType, requiredDose, availableDose} as Vaccine)
      .subscribe(vaccine => this.vaccines?.push(vaccine));
  }
}
