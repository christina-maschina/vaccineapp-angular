import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {

  @Input() manufacturerName: string | undefined;
  message: string | undefined;

  constructor() {
  }

  ngOnInit(): void {
    this.message = 'Cjepivo trenutno nije odobreno od strane EMA-e';
  }

}
