import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchSideEffectComponent } from './search-side-effect.component';

describe('SearchSideEffectComponent', () => {
  let component: SearchSideEffectComponent;
  let fixture: ComponentFixture<SearchSideEffectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchSideEffectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchSideEffectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
