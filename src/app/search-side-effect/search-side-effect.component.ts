import {Component, OnInit} from '@angular/core';
import {SideEffectService} from '../services/side-effect.service';
import {SideEffect} from '../models/SideEffect';
import {Router} from '@angular/router';
import {VaccineService} from '../services/vaccine.service';
import {Vaccine} from '../models/vaccine';

@Component({
  selector: 'app-search-side-effect',
  templateUrl: './search-side-effect.component.html',
  styleUrls: ['./search-side-effect.component.css']
})
export class SearchSideEffectComponent implements OnInit {
  sideEffects: SideEffect[] | undefined;
  selectedSideEffect: SideEffect | undefined;
  vaccine: Vaccine | undefined;

  constructor(private sideEffectService: SideEffectService,
              private router: Router,
              private vaccineService: VaccineService) {
  }

  ngOnInit(): void {
  }

  search(longDescription: string): void {

    if (!longDescription) {
      return;
    }

    this.sideEffectService.search(longDescription)
      .subscribe(sideEffects => this.sideEffects = sideEffects);
  }

  onSelect(sideEffect: SideEffect): void {
    this.selectedSideEffect = sideEffect;
    this.vaccineService.getById(sideEffect.vaccine_id).subscribe(vaccine => this.vaccine = vaccine);
    console.log(sideEffect.vaccine_id);
    console.log(this.vaccine?.manufacturerName);
    this.router.navigate(['searchDetails/', this.vaccine?.manufacturerName]);
  }

}
